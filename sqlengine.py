import os
import re
import sys

def isbalanced(string):
    balance = 0
    for i in string:
        if i=='(':
            balance += 1
        elif i==')':
            balance -= 1
        if balance<0:
            return False
    return True

def parse(sql):
    
    sql = args[1].strip(';')
    fields = ""
    tables = ""
    conditions = ""
    wheresplit = re.split(r'\s+[wW][hH][eE][rR][eE]\s+|\s+[wW][hH][eE][rR][eE]\s*$|^\s*[wW][hH][eE][rR][eE]\s+',sql)
    if len(wheresplit)>2 or len(wheresplit)==0:
        print "WHERE error"
        return

    elif len(wheresplit)==2:
        if wheresplit[0]=="" or wheresplit[1]=="":
            print "WHERE error"
            return
        else:
            conditions = wheresplit[1]
            sql = wheresplit[0]
    
    fromsplit = re.split(r'\s+[fF][rR][oO][mM]\s+|\s+[Ff][Rr][Oo][Mm]\s*$|^\s*[fF][rR][oO][mM]\s+',sql)
    if len(fromsplit) !=2:
        print "FROM error"
        return
    else:
        tables = fromsplit[1]
        selectsplit = re.split(r'^\s*[sS][eE][lL][eE][cC][tT]\s+',fromsplit[0])
        if len(selectsplit) !=2:
            print "SELECT error"
            return
        elif selectsplit[0]!="":
            print "SELECT error"
            return
        else:
            fields = selectsplit[1]
    
    
    # Checking if tables came in correct format

    # removed trailing paranthesis till balanced
    left,right = 0,len(tables)-1
    while left<right and tables[left] == '(' and tables[right] == ')':
        if left+1<=right-1 and not isbalanced(tables[left+1:right]):
            break
        left += 1
        right -= 1
    tables = tables[left:right+1]

    tables = tables.split(',')
    for i in xrange(len(tables)):
        table = tables[i]
        left,right = 0,len(table)-1
        while left<right and table[left] == '(' and table[right] == ')':
            if left+1<=right-1 and not isbalanced(table[left+1:right]):
                break
            left += 1
            right -= 1
        table = table[left:right+1]
        if '(' in table or ')' in table:
            print "Table name should not contain paranthesis"
            return
        if ' ' in table:
            print "Table name must not contain spaces"
            return
        tables[i] = table
    
    # Checking if fields came in correct format
    left,right = 0,len(fields)-1
    while left<right and fields[left] == '(' and fields[right] == ')':
        if left+1<=right-1 and not isbalanced(fields[left+1:right]):
                break
        left += 1
        right -= 1
    fields = fields[left:right+1]

    if ',' not in fields:
        if '(' not in fields and ')' not in fields:
            fields = [[fields,"ALL"]]
        elif '(' in fields and fields[-1]==')':
            func = fields.split('(')[0]
            fields = "(".join(fields.split('(')[1:])
            fields = fields[:-1]
            left,right = 0,len(fields)-1
            while left<right and fields[left] == '(' and fields[right] == ')':
                if left+1<=right-1 and not isbalanced(fields[left+1:right]):
                    break
                left += 1
                right -= 1
            fields = fields[left:right+1]
            if '(' in fields or ')' in fields:
                print "Field name should not contain paranthesis"
                return
            fields = [[fields,func],]
        else:
            print "Unbalanced Paranthesis"
            return
    else:
        fields = fields.split(',')
        for i in range(len(fields)):
            field = fields[i]
            left,right = 0,len(field)-1
            while left<right and field[left] == '(' and field[right] == ')':
                if left+1<=right-1 and not isbalanced(field[left+1:right]):
                    break
                left += 1
                right -= 1
            field = field[left:right+1]
            
            if '(' not in field and ')' not in field:
                field = [field,"ALL"]
            elif '(' in field and field[-1]==')':
                func = field.split('(')[0]
                field = "(".join(field.split('(')[1:])
                field = field[:-1]
                left,right = 0,len(field)-1
                while left<right and field[left] == '(' and field[right] == ')':
                    if left+1<=right-1 and not isbalanced(field[left+1:right]):
                        break
                    left += 1
                    right -= 1
                field = field[left:right+1]
                if '(' in field or ')' in field:
                    print "Field name should not contain paranthesis"
                    return
                field = [field,func]
            else:
                print "Unbalanced Paranthesis"
                return
            fields[i] = field

    aggregate = 0
    distinct = 0
    project = 0
    
    for i in range(len(fields)):
        field = fields[i][0].strip(' ')
        func = fields[i][1].strip(' ')
        if field == '*' and func.lower() != "all":
            print "Here Functions are not supoorted on *"
            return
        if func.lower() in ["avg","max","min","sum"]:
            aggregate += 1
        elif func.lower() == "all":
            project += 1
        elif func.lower() == "distinct":
            distinct += 1
        else:
            print "%s function is not supported here."%(func)
            return
        fields[i] = [field,func]

    #print "Fields =>",fields
    #print "Tables =>",tables
    #print "conditions =>",conditions
    #print "aggregate =",aggregate
    #print "distinct =",distinct
    #print "project =",project

    Query(fields,tables,conditions)

def GetTableInfo(File):

    fptr = open(File,"r+")
    data = fptr.readlines()
    size = len(data)
    lptr = 0
    info = {}
    while lptr < size:
        line = data[lptr].strip('\n').strip('\r')
        if line == "<begin_table>":
            lptr += 1
            line = data[lptr].strip('\n').strip('\r')
            table_name = line
            info[table_name] = ()
            lptr += 1
            line = data[lptr].strip('\n').strip('\r')
            while line != "<end_table>":
                info[table_name] += (line,)
                lptr += 1
                line = data[lptr].strip('\n').strip('\r')
        lptr += 1

    return info


def Query(fields,tables,conditions):

    "updating conditions to match type insensitively"
    
    conditions = conditions.replace('\"','"')
    conditions = conditions.replace('=','==')
    conditions = conditions.replace('!==','!=')
    conditions = conditions.replace('>==','>=')
    conditions = conditions.replace('<==','<=')
    temp = conditions
    conditions = ""
    flag = False
    mid = ""
    
    for i in temp:
        if i=='"':
            if flag:
                conditions += mid
                mid = ""
            flag = not flag
        else:
            if flag:
                mid += i
            else:
                conditions += i
    if flag:
        print "Condition Error"
        return

    info = GetTableInfo('metadata.txt')

    for table in tables:
        if table not in info:
            print "%s table doesn't exists."%(table)
            return

    if sorted(list(set(tables)))!=sorted(tables):
        print "Since Aliasing is not implemented a table cant be joined with itself"
        return

    new_fields = []

    for field,func in fields:
        if field == "*":
            for table in tables:
                for field in info[table]:
                    new_fields.append([table+"."+field,"ALL"])
        else:
            new_fields.append([field,func])

    fields = new_fields

    new_fields = []
    for field,func in fields:
        parts = field.split('.')
        if len(parts)>2:
            print "Invalid Field Name"
            return
        elif len(parts) == 2:
            if parts[0] not in info:
                print "Table %s doesn't exist"%(parts[0])
                return
            if parts[1] not in info[parts[0]]:
                print "Table %s doesn't have the attribute %s"%(parts[0],parts[1])
                return
            new_fields.append([field,func])
        elif len(parts) == 1:
            cnt = 0
            for table in tables:
                if field in info[table]:
                    table_name = table
                    cnt += 1
            if cnt == 0:
                print "No table conatins the field %s"%(field)
                return
            elif cnt>1:
                print "Multiple tables contain the attribute %s"%(field)
                return
            else:
                new_fields.append([table_name+"."+field,func])


    join = [(),]
    for table in tables:
        temp = []
        fptr = open(table+".csv","r+")
        for line in fptr.readlines():
            fields = tuple(map(int,line.strip('\n').strip('\r').split(',')))
            for record in join:
                temp.append(record+fields)
        join = []
        for record in temp:
            join.append(record)

    "A simple hack to evaluate any condition"
    class temp:
        pass

    attr = {}
    for table in tables:
        exec("%s = temp()"%(table))
        for field in info[table]:
            if field in attr:
                attr[field].append(table)
            else:
                attr[field] = [table,]

    valid = {}
    for field in attr:
        if len(attr[field]) == 1:
            valid[field] = attr[field][0]+"."+field

    index = {}
    val = 0
    for table in tables:
        for field in info[table]:
            index[table+"."+field] = val
            val += 1

    if conditions == "":
        conditions = "True"

    ignore = []
    # Handling join condition (unnecessary but part of assignment)
    if len(tables) == 2:

        temp = conditions
        left,right = 0,len(conditions)-1
        while left<right and conditions[left] == '(' and conditions[right] == ')':
            if left+1<=right-1 and not isbalanced(conditions[left+1:right]):
                break
            left += 1
            right -= 1

        conditions = conditions[left:right+1]

        conditions = re.split(r'==',conditions)

        if not (len(conditions)!=2 or '<' in conditions[0] or '>' in conditions[0] or '!' in conditions[0] or \
                '<' in conditions[1] or '>' in conditions[1] or '!' in conditions[1] ):
            for i in range(2):
                left,right = 0,len(conditions[i])-1
                while left<right and conditions[i][left] == '(' and conditions[i][right] == ')':
                    if left+1<=right-1 and not isbalanced(conditions[i][left+1:right]):
                        break
                    left += 1
                    right -= 1
                conditions[i] = conditions[i][left:right+1]
                if '(' in conditions[i] or ')' in conditions[i]:
                    print "Unbalanced Paranthesis"
                    return

            print conditions
            for i in range(2):
                field = conditions[i]
                parts = field.split('.')
                if len(parts)>2:
                    print "Invalid Field Name"
                    return
                elif len(parts) == 2:
                    if parts[0] not in info:
                        print "Table %s doesn't exist"%(parts[0])
                        return
                    if parts[1] not in info[parts[0]]:
                        print "Table %s doesn't have the attribute %s"%(parts[0],parts[1])
                        return
                elif len(parts) == 1:
                    cnt = 0
                    for table in tables:
                        if field in info[table]:
                            table_name = table
                            cnt += 1
                    if cnt == 0:
                        print "No table conatins the field %s"%(field)
                        return
                    elif cnt>1:
                        print "Multiple tables contain the attribute %s"%(field)
                        return
                    else:
                        conditions[i] = table_name+"."+field
            ignore.append(conditions[1])
            conditions = '=='.join(conditions)
        else:
            conditions = temp

    try:
        result = []
        for record in join:
            s = 0
            for table in tables:
                i = 0
                for field in info[table]:
                    exec("%s.%s = %d"%(table,info[table][i],record[s]))
                    i += 1
                    s += 1
            for i in valid:
                exec("%s = %s"%(i,valid[i]))

            if eval(conditions):
                result.append(record)
    except SyntaxError:
        print "Unbalanced Paranthesis or Unknown operators"
        return
    except NameError:
        print "Unknown fields or clashing fields used in condition"
        return
    except:
        print "Invalid SQL"
        return
    
    fields = new_fields

    columnresult = [[] for field in fields]
    for record in result:
        ind = 0
        for field,func in fields:
            columnresult[ind].append(record[index[field]])
            ind += 1

    temp = []
    for i in range(len(fields)):
        if fields[i][0] not in ignore:
            temp.append(fields[i][0])
    print ','.join(temp)

    ind = 0
    for field,func in fields:
        if func.lower() != "all":
            if func.lower() == "min":
                columnresult[ind] = [min(columnresult[ind]),]
            elif func.lower() == "max":
                columnresult[ind] = [max(columnresult[ind]),]
            elif func.lower() == "sum":
                columnresult[ind] = [sum(columnresult[ind]),]
            elif func.lower() == "avg":
                columnresult[ind] = [sum(columnresult[ind])/float(len(columnresult[ind])),]
            elif func.lower() == "distinct":
                columnresult[ind] = sorted(list(set(columnresult[ind])))
            else:
                print "Sorry , Some Bug in my Program"
                return
        ind += 1

    maxlen = max([len(column) for column in columnresult])
    
    for i in range(len(columnresult)):
        columnresult[i] += ['<empty>']*(maxlen-len(columnresult[i]))

    for i in range(maxlen):
        temp = []
        for j in range(len(columnresult)):
            if fields[j][0] not in ignore:
                temp.append(str(columnresult[j][i]))
        print ','.join(temp)
    print "Succesfull Query with %d columns"%(maxlen)

args = sys.argv

if len(args)!=2:
    print 'Invalid Number of Arguments'
    print 'Usage: python %s "SELECT table1.A from table1;"'%(args[0])
else:
    parse(args[1])
    exit(0)
